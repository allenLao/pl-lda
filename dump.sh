#!/usr/bin/env sh
model=$1
./topics.py $model/final_lan_0.beta ap/vocab.txt 20 > $model/topicWord_lan0
./topics.py $model/final_lan_1.beta ap/vocab.txt 20 > $model/topicWord_lan1
vimdiff $model/topicWord_lan0 $model/topicWord_lan1
