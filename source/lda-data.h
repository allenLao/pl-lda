#ifndef LDA_DATA_H
#define LDA_DATA_H

#include <stdio.h>
#include <stdlib.h>

#include "lda.h"
#include "utils.h"

#define OFFSET 0;                  // offset for reading data

corpus* read_data(char* data_filename);
corpus* read_data_lan(char* data_filename, int lan);

int max_corpus_length(corpus* c);

void save_gamma(char* filename,
                double** gamma,
                int num_docs,
                int num_topics);

void corr_save_gamma(char* filename,
                    double*** gamma,
                    int num_docs,
                    int num_topics,
                    int num_languages);

void online_save_gamma(char* filename,
                       double** gamma,
                       int begin,
                       int end,
                       int num_topics);


void write_word_assignment(FILE** f,
                           document** doc,
                           double*** phi,
                           lda_model* model);
#endif
