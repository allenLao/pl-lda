// (C) Copyright 2004, David M. Blei (blei [at] cs [dot] cmu [dot] edu)

// This file is part of LDA-C.

// LDA-C is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your
// option) any later version.

// LDA-C is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

#include "lda-model.h"

/*
 * compute MLE lda model from sufficient statistics
 *
 */

double lda_mle(lda_model* model, lda_suffstats* ss, int estimate_alpha, int estimate_eta)
{
    int k, w, l;
    double* ss_eta = malloc(sizeof(double) * model->num_languages);
    
    //keep the topic/word likelihood
    double  lambdas_lik = 0;
    
    for(l =0; l < model->num_languages; l++){
        ss_eta[l] = 0;
        for (k = 0; k < model->num_topics; k++)
        {
            model->sum_lambda[l][k] = ss->class_total[l][k];
            for (w = 0; w < model->num_terms[l]; w++)
            {
                model->lambda[l][k][w] = ss->class_word[l][k][w];
                
                model->elog_beta[l][k][w] = digamma(model->lambda[l][k][w])- digamma(model->sum_lambda[l][k]);

                ss_eta[l] += model->elog_beta[l][k][w];
            }
        }
    }
    if (estimate_alpha == 1)
    {
        
        newton_est_alpha(model->alpha, ss->alpha_suffstats, ss->num_docs, model->num_topics);
        printf("new alpha: ");
        for(k =0; k <model->num_topics; k++)
        {
            printf("%f ", model->alpha[k]);
        }
        printf("\n");
    }
    if(estimate_eta == 1)
    {
        for ( l =0; l < model->num_languages; l++)
        {
            model->etas[l] = opt_alpha(ss_eta[l], model->num_topics , model->num_terms[l], 1.0);
            printf("new eta_%d %f\n",l, model->etas[l]);
        }
        //free
        free(ss_eta);
    }
    //
    //partial likelihood collcetion
    for(l =0; l < model->num_languages; l++)
    {
        lambdas_lik += lgamma(model->num_terms[l] * model->etas[l])
        - model->num_terms[l] * lgamma(model->etas[l]);
        
        for (k = 0; k < model->num_topics; k++)
        {
            for (w = 0; w < model->num_terms[l]; w++)
                lambdas_lik += (model->etas[l] - model->lambda[l][k][w]) * model->elog_beta[l][k][w]
                + lgamma(model->lambda[l][k][w]);
            lambdas_lik -= lgamma(model->sum_lambda[l][k]);
        }
    }

    return lambdas_lik;
}


double online_lda_mle(lda_model* model, lda_suffstats* ss, double roh, int estimate_alpha, int estimate_eta, int num_docs)
{
    int k; int w, l;
    //keep the topic/word likelihood
    double  lambdas_lik = 0;
    double doc_rate = 1.0 * num_docs/ss->num_docs;
    
    for(l =0; l < model->num_languages; l++){
        for (k = 0; k < model->num_topics; k++)
        {
            model->sum_lambda[l][k] *= (1- roh);
            model->sum_lambda[l][k] += ss->class_total[l][k] * doc_rate * roh;
            for (w = 0; w < model->num_terms[l]; w++)
            {
                model->lambda[l][k][w] *= (1- roh);
                model->lambda[l][k][w] += ss->class_word[l][k][w] * doc_rate * roh;
                model->elog_beta[l][k][w] = digamma(model->lambda[l][k][w])- digamma(model->sum_lambda[l][k]);
            }
        }
        
    }
    
    if (estimate_alpha == 1)
    {
        newton_est_alpha_weight(model->alpha, ss->alpha_suffstats, ss->num_docs, model->num_topics, roh);
        printf("new alpha: ");
        for(k =0; k <model->num_topics; k++)
        {
            printf("%f ", model->alpha[k]);
        }
        printf("\n");
    }
    //
    //partial likelihood collcetion
    for(l =0; l < model->num_languages; l++)
    {
        lambdas_lik += lgamma(model->num_terms[l] * model->etas[l])
        - model->num_terms[l] * lgamma(model->etas[l]);
        
        for (k = 0; k < model->num_topics; k++)
        {
            for (w = 0; w < model->num_terms[l]; w++)
                lambdas_lik += (model->etas[l] - model->lambda[l][k][w]) * model->elog_beta[l][k][w]
                + lgamma(model->lambda[l][k][w]);
            lambdas_lik -= lgamma(model->sum_lambda[l][k]);
        }
    }
    
    return lambdas_lik;
}


/*
 * allocate sufficient statistics
 *
 */

lda_suffstats* new_lda_suffstats(lda_model* model)
{
    int i,j,l;
    
    lda_suffstats* ss = malloc(sizeof(lda_suffstats));
    ss->class_total = malloc(sizeof(double*) * model->num_languages);
    ss->class_word = malloc(sizeof(double**) * model->num_languages);
    ss->alpha_suffstats = malloc(sizeof(double) * model->num_topics);
    for(l = 0; l < model->num_languages; l++){
        ss->class_word[l] = malloc(sizeof(double*) * model->num_topics);
        ss->class_total[l] = malloc(sizeof(double)* model->num_topics);
        for (i = 0; i < model->num_topics; i++)
        {
            ss->class_total[l][i] = 0;
            ss->class_word[l][i] = malloc(sizeof(double*)* model->num_terms[l]);
            for (j = 0; j < model->num_terms[l]; j++)
            {
                ss->class_word[l][i][j] = 0;
            }
        }
    }
    return(ss);
}


/*
 * various intializations for the sufficient statistics
 *
 */

void zero_initialize_ss(lda_suffstats* ss, lda_model* model)
{
    
    int k, w, l;
    for (k = 0; k < model->num_topics; k++)
    {
        for(l = 0; l < model->num_languages; l++){
            // ss->class_total[l][k] = 0;
            ss->class_total[l][k] = model->etas[l]*model->num_terms[l];
            
            for (w = 0; w < model->num_terms[l]; w++)
            {
                ss->class_word[l][k][w] = model->etas[l];
                // ss->class_word[l][k][w] = 0;
            }
        }
        ss->alpha_suffstats[k] = 0;
        
    }
    ss->num_docs = 0;
}

/*
 * various intializations for the sufficient statistics
 *
 */

void online_initialize_ss(lda_suffstats* ss, lda_model* model, double roh)
{
    
    int k, w, l;
    for (k = 0; k < model->num_topics; k++)
    {
        for(l = 0; l < model->num_languages; l++){
            ss->class_total[l][k] = model->etas[l]*model->num_terms[l] * roh;
            
            for (w = 0; w < model->num_terms[l]; w++)
            {
                ss->class_word[l][k][w] = model->etas[l] * roh;
            }
        }
        ss->alpha_suffstats[k] = 0;
        
    }
    ss->num_docs = 0;
}

void random_initialize_ss(lda_suffstats* ss, lda_model* model)
{
    int num_topics = model->num_topics;
    int num_languages = model->num_languages;
    int* num_terms = model->num_terms;
    int l, k, n;
    for (k = 0; k < num_topics; k++)
    {
        for(l = 0; l < num_languages; l++){
            for (n = 0; n < num_terms[l]; n++)
            {
                ss->class_word[l][k][n] += 1.0/num_terms[l] + myrand();
                ss->class_total[l][k] += ss->class_word[l][k][n];
            }
        }
    }
}

/*
 * allocate new lda model
 *
 */

lda_model* new_lda_model(int num_languages, int* num_terms, int num_topics)
{
    int i,j,l;
    lda_model* model;
    
    model = malloc(sizeof(lda_model));
    model->num_topics = num_topics;
    model->num_terms = num_terms;
    model->num_languages = num_languages;
    model->alpha = malloc(sizeof(double) * num_topics);
    model->etas = malloc(sizeof(double*) * num_languages);
    for (i =0; i < num_topics; i++)
    {
        model->alpha[i] = 1.0;
    }
    model->lambda = malloc(sizeof(double**) * num_languages);
    model->elog_beta = malloc(sizeof(double**) * num_languages);
    model->sum_lambda = malloc(sizeof(double*) * num_languages);
    
    for(l = 0; l < num_languages; l++){
        model->lambda[l] = malloc(sizeof(double*) * num_topics);
        model->sum_lambda[l] = malloc(sizeof(double*) * num_topics);
        model->elog_beta[l] = malloc(sizeof(double*) * num_topics);
        for (i = 0; i < num_topics; i++)
	    {
            model->lambda[l][i] = malloc(sizeof(double) * num_terms[l]);
            model->elog_beta[l][i] = malloc(sizeof(double) * num_terms[l]);
            model->sum_lambda[l][i] = 0;
            for (j = 0; j < num_terms[l]; j++)
            {
                model->lambda[l][i][j] = 0;
                model->elog_beta[l][i][j] = 0;
            }
	    }
    }
    
    return(model);
}


/*
 * deallocate new lda model
 *
 */

void free_lda_model(lda_model* model)
{
    int i,l;
    for(l = 0; l < model->num_languages; l++){
        for (i = 0; i < model->num_topics; i++)
    	{
            free(model->lambda[l][i]);
            free(model->elog_beta[l][i]);
        }
        free(model->lambda[l]);
        free(model->elog_beta[l]);
    }
    free(model->alpha);
    free(model->etas);
    free(model->lambda);
}


/*
 * save an lda model
 *
 */

void save_lda_model(lda_model* model, char* model_root)
{
    char filename[100];
    char betas_file[100];
    FILE* fileptr, *bfileptr;
    int l, i, j;
    
    for (l = 0; l < model->num_languages; l++){
        sprintf(filename, "%s_lan_%d.lambda", model_root, l);
        sprintf(betas_file, "%s_lan_%d.beta", model_root, l);
        fileptr = fopen(filename, "w");
        bfileptr = fopen(betas_file, "w");
	    for (i = 0; i < model->num_topics; i++)
	    {
            for (j = 0; j < model->num_terms[l]; j++)
            {
                fprintf(fileptr, " %5.10f", model->lambda[l][i][j]);
                fprintf(bfileptr, " %5.10f", model->lambda[l][i][j]/model->sum_lambda[l][i]);
            }
            fprintf(fileptr, "\n");
            fprintf(bfileptr, "\n");
	    }
        fflush(fileptr);
        fflush(bfileptr);
        fclose(fileptr);
        fclose(bfileptr);
	}
    
    sprintf(filename, "%s.other", model_root);
    fileptr = fopen(filename, "w");
    fprintf(fileptr, "num_topics %d\n", model->num_topics);
    fprintf(fileptr, "num_languages %d\n", model->num_languages);
    for(l = 0; l < model->num_languages; l++){
        fprintf(fileptr, "num_terms_language_%d %d\n", l ,model->num_terms[l]);
    }
    for(l = 0; l < model->num_languages; l++){
        fprintf(fileptr, "etas_%d %f\n", l ,model->etas[l]);
    }
    
    for(i = 0; i < model->num_topics; i++)
        fprintf(fileptr, "alpha_%d %5.10f\n",i , model->alpha[i]);
    fflush(fileptr);
    fclose(fileptr);
}


lda_model* load_lda_model(char* model_root)
{
    char filename[100];
    FILE* fileptr;
    int i, j, l, num_topics, num_languages, tl;
    int* num_terms;
    double* alphas;
    double* eta;
    float x;
    
    sprintf(filename, "%s.other", model_root);
    printf("loading %s\n", filename);
    fileptr = fopen(filename, "r");
    fscanf(fileptr, "num_topics %d\n", &num_topics);
    fscanf(fileptr, "num_languages %d\n", &num_languages);
    num_terms = malloc(sizeof(int) * num_languages);
    for(l = 0; l < num_languages; l++){
        fscanf(fileptr, "num_terms_language_%d %d\n", &tl, num_terms+l);
    }
    eta = malloc(sizeof(double) * num_languages);
    for(l = 0; l < num_languages; l++){
        fscanf(fileptr, "etas_%d %lf\n",  &tl , eta+l);
    }
    alphas = malloc(sizeof(double)*num_topics);
    for(i = 0; i < num_topics; i++){
        fscanf(fileptr, "alpha_%d %lf\n", &tl, alphas+i);
    }
    fclose(fileptr);
    
    lda_model* model = new_lda_model(num_languages, num_terms, num_topics);
    model->alpha = alphas;
    model->etas = eta;
    for(l = 0; l < num_languages; l++){
        sprintf(filename, "%s/final_lan_%d.lambda", model_root, l);
        fileptr = fopen(filename, "r");
        printf("loading %s\n", filename);
        for (i = 0; i < num_topics; i++){
            for (j = 0; j < num_terms[l]; j++) {
                fscanf(fileptr, "%f", &x);
                model->lambda[l][i][j] = x;
                model->sum_lambda[l][i] += model->lambda[l][i][j];
            }
        }
        fclose(fileptr);
    }
    
    
    return(model);
}
