// (C) Copyright 2004, David M. Blei (blei [at] cs [dot] cmu [dot] edu)

// This file is part of LDA-C.

// LDA-C is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your
// option) any later version.

// LDA-C is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

#include "lda-inference.h"

/*
 * variational inference
 *
 */

double lda_inference(document** doc, lda_model* model, double* var_gamma, double*** phi)
{
    double converged = 1;
    double phisum = 0, likelihood = 0;
    double likelihood_old = 0;
    double** oldphi[model->num_languages];
    int l, k, n, var_iter;
    double digamma_gam[model->num_topics];
    int tuple_total = 0;
    // compute posterior dirichlet

    for(l =0; l < model->num_languages; l++){
        tuple_total += doc[l]->total;
    }

    for(l =0; l < model->num_languages; l++){
        oldphi[l] = malloc(sizeof(double*) * doc[l]->length);
        for (n = 0; n < doc[l]->length; n++)
        {
            oldphi[l][n] = malloc(sizeof(double)* model->num_topics);
        }
    }

    for (k = 0; k < model->num_topics; k++)
    {
        var_gamma[k] = model->alpha[k] + (tuple_total/((double) model->num_topics));
        digamma_gam[k] = digamma(var_gamma[k]);
        for( l =0; l < model->num_languages; l++){
            for (n = 0; n < doc[l]->length; n++)
                phi[l][n][k] = 1.0/model->num_topics;
        }
    }
    var_iter = 0;

    while ((converged > VAR_CONVERGED) &&
           ((var_iter < VAR_MAX_ITER) || (VAR_MAX_ITER == -1)))
    {
        var_iter++;
        for(l =0; l < model->num_languages; l++){
            for (n = 0; n < doc[l]->length; n++)
            {
                phisum = 0;
                for (k = 0; k < model->num_topics; k++)
                {
                    oldphi[l][n][k] = phi[l][n][k];
                    phi[l][n][k] = digamma_gam[k] +
                    model->elog_beta[l][k][doc[l]->words[n]];

                    if (k > 0)
                        phisum = log_sum(phisum, phi[l][n][k]);
                    else
                        phisum = phi[l][n][k]; // note, phi is in log space
                }
                //normal
                for (k = 0; k < model->num_topics; k++)
                {
                    phi[l][n][k] = exp(phi[l][n][k] - phisum);
                }
            }
        }


        for(l =0; l < model->num_languages; l++){
            for (n = 0; n < doc[l]->length; n++)
            {
                for (k = 0; k < model->num_topics; k++)
                {
                    var_gamma[k] +=
                    doc[l]->counts[n]*(phi[l][n][k] - oldphi[l][n][k]);
                }

            }
        }

        for (k = 0; k < model->num_topics; k++)
        {
            digamma_gam[k] = digamma(var_gamma[k]);
        }
        // !!! a lot of extra digamma's here because of how we're computing it
        // !!! but its more automatically updated too.
        likelihood = compute_likelihood(doc, model, phi, var_gamma);
        assert(!isnan(likelihood));
        converged = (likelihood_old - likelihood) / likelihood_old;
        likelihood_old = likelihood;
    }

    //release local variables
    for(l =0; l < model->num_languages; l++){
        for (n = 0; n < doc[l]->length; n++)
        {
            free(oldphi[l][n]);
        }
    }
    return(likelihood);
}




/*
 * compute likelihood bound
 *
 */

double
compute_likelihood(document** doc, lda_model* model, double*** phi, double* var_gamma)
{
    double likelihood = 0, digsum = 0, var_gamma_sum = 0, dig[model->num_topics];
    double alpha_sum = 0;
    int l, k, n;

    for (k = 0; k < model->num_topics; k++)
    {
        dig[k] = digamma(var_gamma[k]);
        var_gamma_sum += var_gamma[k];
        alpha_sum += model->alpha[k];
    }
    digsum = digamma(var_gamma_sum);


    likelihood = lgamma(alpha_sum) - (lgamma(var_gamma_sum));

    for (k = 0; k < model->num_topics; k++){
        likelihood -= lgamma(model->alpha[k]);
        likelihood += lgamma(var_gamma[k]);
        likelihood += (model->alpha[k] - var_gamma[k]) * (dig[k] - digsum);

        for(l = 0; l < model->num_languages; l++){
            for (n = 0; n < doc[l]->length; n++){
                if (phi[l][n][k] > 0){
                    likelihood += doc[l]->counts[n]*
                    phi[l][n][k]*((dig[k] - digsum) - log(phi[l][n][k])
                                  + model->elog_beta[l][k][doc[l]->words[n]]);
                }
            }
        }
    }
    return(likelihood);
}
