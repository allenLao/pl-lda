#ifndef LDA_ESTIMATE_H
#define LDA_ESTIMATE_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <time.h>

#include "lda.h"
#include "lda-data.h"
#include "lda-inference.h"
#include "lda-model.h"
#include "lda-alpha.h"
#include "utils.h"
#include "cokus.h"

int LAG = 5;

float EM_CONVERGED;
int EM_MAX_ITER;
int ESTIMATE_ALPHA;
int ESTIMATE_ETA;
double INITIAL_ALPHA;
double INITIAL_ETA;
double TAU;
double KAPPA;
int MINIBATCH;
int NTOPICS;
int NLANGUAGES;
int NDOCUMENTS;

double doc_e_step(document** doc,
                  double* gamma,
                  double*** phi,
                  lda_model* model,
                  lda_suffstats* ss);

void run_em(char* start,
            char* directory,
            corpus** corpus);

void online(char* start,
            char* directory,
            corpus** corpus);

void read_settings(char* filename);

void infer(char*, char* save, corpus** corpus);

#endif


