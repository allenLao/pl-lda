// (C) Copyright 2004, David M. Blei (blei [at] cs [dot] cmu [dot] edu)

// This file is part of LDA-C.

// LDA-C is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your
// option) any later version.

// LDA-C is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the imp

// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

#include "lda-estimate.h"

/*
 * perform inference on a document and update sufficient statistics
 *
 */

double doc_e_step(document** tuple, double* gamma, double*** phi,
                  lda_model* model, lda_suffstats* ss)
{
    double likelihood;
    int n, k, l;

    // posterior inference

    likelihood = lda_inference(tuple, model, gamma, phi);

    // update sufficient statistics

    double gamma_sum = sum_up(gamma, model->num_topics);
    for (k = 0; k < model->num_topics; k++)
    {
        ss->alpha_suffstats[k] += digamma(gamma[k]) - digamma(gamma_sum);
    }

    for( l = 0; l < NLANGUAGES; l++){
        for (n = 0; n < tuple[l]->length; n++)
        {
            for (k = 0; k < model->num_topics; k++)
            {
                ss->class_word[l][k][tuple[l]->words[n]] += tuple[l]->counts[n]*phi[l][n][k];
                ss->class_total[l][k] += tuple[l]->counts[n]*phi[l][n][k];
            }
        }
    }
    ss->num_docs = ss->num_docs + 1;
    return(likelihood);
}

/*
 * run_em
 *
 */

void run_em(char* start, char* directory, corpus** m_corpus)
{

    int d, n, l, k, tokens;
    clock_t begin, stop, sub_begin, sub_stop;
    begin = clock();
    lda_model *model = NULL;
    //L * N * K
    double **var_gamma, ***phi;

    // allocate variational parameters

    var_gamma = malloc(sizeof(double*)* NDOCUMENTS);
    for (d = 0; d < NDOCUMENTS; d++)
        var_gamma[d] = malloc(sizeof(double) * NTOPICS);


    phi = malloc(sizeof(double**)*NLANGUAGES);
    for(l = 0; l < NLANGUAGES; l++){
        int max_length = max_corpus_length(m_corpus[l]);
	    phi[l] = malloc(sizeof(double*)*max_length);
	    for (n = 0; n < max_length; n++)
            phi[l][n] = malloc(sizeof(double) * NTOPICS);
    }

    // initialize model

    char filename[100];

    lda_suffstats* ss = NULL;
    int num_terms_array[NLANGUAGES];
    for(l = 0; l < NLANGUAGES; l++){
        num_terms_array[l] = m_corpus[l]->num_terms;
    }

    if (strcmp(start, "random")==0) {
        model = new_lda_model(NLANGUAGES,num_terms_array, NTOPICS);

        ss = new_lda_suffstats(model);
        for (k =0; k < NTOPICS; k++)
            model->alpha[k] = INITIAL_ALPHA;
        for (l =0; l < NLANGUAGES; l++)
            model->etas[l] = INITIAL_ETA;
        random_initialize_ss(ss, model);
        lda_mle(model, ss, 0, 0);


    }
    else
    {
        model = load_lda_model(start);
        ss = new_lda_suffstats(model);
    }

    sprintf(filename,"%s/000",directory);
    save_lda_model(model, filename);

    // run expectation maximization
    int i = 0;
    double likelihood, likelihood_old = 0, converged = 1, perplexity =0;
    sprintf(filename, "%s/likelihood.dat", directory);
    FILE* likelihood_file = fopen(filename, "w");
    char s_perp[100];
    sprintf(s_perp, "%s/perplexity.dat", directory);
    FILE* perplexity_file = fopen(s_perp, "w");
	document** tuple = malloc(sizeof(document*) * NLANGUAGES);
    while (((converged < 0) || (converged > EM_CONVERGED) || (i <= 2)) && (i <= EM_MAX_ITER))
    {
        sub_begin = clock();
        i++; printf("**** em iteration %d ****\n", i);
        zero_initialize_ss(ss, model);

        tokens =0;
        // e-step
        likelihood = 0;
        for (d = 0; d < NDOCUMENTS; d++)
        {
            for(l = 0; l < NLANGUAGES; l++){
                tuple[l] = &(m_corpus[l]->docs[d]);
                tokens += m_corpus[l]->docs[d].total;
            }
            if ((d % 1000) == 0) printf("document %d ...\n",d);
            likelihood += doc_e_step(tuple,var_gamma[d], phi, model, ss);
        }
        // m-step

        lda_mle(model, ss, ESTIMATE_ALPHA, ESTIMATE_ETA);
        // check for convergence
        perplexity = exp(-likelihood/tokens);
        converged = fabs((likelihood_old - likelihood) / (likelihood_old));
        printf("likelihood, perplexity: %f, %f\n", likelihood, perplexity);
        if (converged < 0) VAR_MAX_ITER = VAR_MAX_ITER * 2;
        likelihood_old = likelihood;

        // output model and likelihood

        fprintf(likelihood_file, "%10.10f\t%5.5e\n", likelihood, converged);
        fprintf(perplexity_file, "%10.10f\n", perplexity);
        fflush(likelihood_file);
        fflush(perplexity_file);
        if ((i % LAG) == 0)
        {
            sprintf(filename,"%s/%03d",directory, i);
            save_lda_model(model, filename);
            sprintf(filename,"%s/%03d.gamma",directory, i);
            save_gamma(filename, var_gamma, NDOCUMENTS, NTOPICS);
        }
        sub_stop = clock();
        printf("the total estimation takes around %d s.\n",(int)(sub_stop - sub_begin)/CLOCKS_PER_SEC);

    }


    // output the final model

    sprintf(filename,"%s/final",directory);
    save_lda_model(model, filename);
    sprintf(filename,"%s/final.gamma",directory);
    save_gamma(filename, var_gamma, NDOCUMENTS, model->num_topics);

    // output the word assignments (for visualization)

    //sprintf(filename, "%s/word-assignments.dat", directory);
    FILE* w_asgn_file[NLANGUAGES];
    char file_names[NLANGUAGES][100];
    for(l = 0; l < NLANGUAGES; l++){
        sprintf(file_names[l], "%s/lan_%d_word-assignments.dat", directory, l);
        w_asgn_file[l]= fopen(file_names[l], "w");
    }
    for (d = 0; d < NDOCUMENTS; d++)
    {
        if ((d % 100) == 0) printf("final e step document %d\n",d);

        for(l = 0; l < NLANGUAGES; l++){
            tuple[l] = &(m_corpus[l]->docs[d]);
        }
        likelihood += lda_inference(tuple, model, (var_gamma[d]), phi);
        write_word_assignment(w_asgn_file, tuple, phi, model);
    }
    for(l = 0; l < NLANGUAGES; l++){

        fclose(w_asgn_file[l]);
    }
    fclose(likelihood_file);
    fclose(perplexity_file);

    //release model
    free_lda_model(model);
    //release vargamma
    for (d = 0; d < NDOCUMENTS; d++)
        free(var_gamma[d]);
    free(var_gamma);

    //release phi
    for(l = 0; l < NLANGUAGES; l++){
        int max_length = max_corpus_length(m_corpus[l]);
	    for (n = 0; n < max_length; n++)
            free(phi[l][n]);
        free(phi[l]);
    }
    free(phi);
    //release ss
    free(ss->alpha_suffstats);
    for (l =0; l < NLANGUAGES; l++)
    {
        for (k =0; k < NTOPICS; k++)
        {
            free(ss->class_word[l][k]);
        }
        free(ss->class_word[l]);
        free(ss->class_total[l]);
    }
    free(ss->class_total);
    free(ss->class_word);
    free(ss);
    stop = clock();
    printf("totally takes around %d s.", (int)(stop - begin)/CLOCKS_PER_SEC);
}


/*
 * read settings.
 *
 */

void read_settings(char* filename)
{
    FILE* fileptr;
    char alpha_action[100];
    char eta_action[100];
    fileptr = fopen(filename, "r");
    fscanf(fileptr, "var max iter %d\n", &VAR_MAX_ITER);
    fscanf(fileptr, "var convergence %f\n", &VAR_CONVERGED);
    fscanf(fileptr, "em max iter %d\n", &EM_MAX_ITER);
    fscanf(fileptr, "em convergence %f\n", &EM_CONVERGED);
    fscanf(fileptr, "alpha %s\n", alpha_action);
    fscanf(fileptr, "eta %s\n", eta_action);
    fscanf(fileptr, "lag %d\n", &LAG);
    if (strcmp(alpha_action, "fixed")==0)
    {
        ESTIMATE_ALPHA = 0;
    }
    else
    {
        ESTIMATE_ALPHA = 1;
    }
    if (strcmp(eta_action, "fixed")==0)
    {
        ESTIMATE_ETA = 0;
        printf("fixed eta\n");
    }
    else
    {
        ESTIMATE_ETA = 1;
    }
    fclose(fileptr);
}


/*
 * inference only
 *
 */

void infer(char* model_root, char* save, corpus** corpus){
    FILE* fileptr;
    char filename[100];
    int i, d, n, l;
    lda_model *model;
    double **var_gamma, likelihood, ***phi;
    document** tuple;

    model = load_lda_model(model_root);
    var_gamma = malloc(sizeof(double*)*(NDOCUMENTS));
    tuple = malloc(sizeof(document*) * NLANGUAGES);
    for (i = 0; i < corpus[0]->num_docs; i++)
        var_gamma[i] = malloc(sizeof(double)*model->num_topics);

    sprintf(filename, "%s-lda-lhood.dat", save);
    fileptr = fopen(filename, "w");

    for (d = 0; d < corpus[0]->num_docs; d++)
    {
        if (((d % 100) == 0) && (d>0)) printf("document %d\n",d);

        phi =  malloc(sizeof(double**) * model->num_languages);
        for(l = 0; l < NLANGUAGES; l++){
            tuple[l] = &corpus[l]->docs[d];
            for (n = 0; n < tuple[l]->length; n++){
                phi[l][n] =  malloc(sizeof(double) * model->num_topics);
            }
        }
        likelihood = lda_inference(tuple, model, var_gamma[d], phi);

        fprintf(fileptr, "%5.5f\n", likelihood);
    }
    fclose(fileptr);
    sprintf(filename, "%s-gamma.dat", save);
    save_gamma(filename, var_gamma, NDOCUMENTS, NTOPICS);
}


/*
 * online learning
 *
 */

void online(char* start, char* directory, corpus** m_corpus)
{

    int d, n, l, k, num_run;
    lda_model *model = NULL;
    //L * N * K
    double **var_gamma, ***phi;
    clock_t clk_begin, clk_stop, sub_begin, sub_stop;
    clk_begin = clock();
    // allocate variational parameters

    var_gamma = malloc(sizeof(double*)* NDOCUMENTS);
    for (d = 0; d < NDOCUMENTS; d++)
        var_gamma[d] = malloc(sizeof(double) * NTOPICS);


    phi = malloc(sizeof(double**)*NLANGUAGES);
    for(l = 0; l < NLANGUAGES; l++){
        int max_length = max_corpus_length(m_corpus[l]);
	    phi[l] = malloc(sizeof(double*)*max_length);
	    for (n = 0; n < max_length; n++)
            phi[l][n] = malloc(sizeof(double) * NTOPICS);
    }

    // initialize model

    char filename[100];

    lda_suffstats* ss = NULL;
    int num_terms_array[NLANGUAGES];
    for(l = 0; l < NLANGUAGES; l++){
        num_terms_array[l] = m_corpus[l]->num_terms;
    }

    if (strcmp(start, "random")==0) {
        model = new_lda_model(NLANGUAGES,num_terms_array, NTOPICS);

        ss = new_lda_suffstats(model);
        for (k =0; k < NTOPICS; k++)
            model->alpha[k] = INITIAL_ALPHA;
        for (l =0; l < NLANGUAGES; l++)
            model->etas[l] = INITIAL_ETA;
        random_initialize_ss(ss, model);
        lda_mle(model, ss, 0, 0);


    }
    else
    {
        model = load_lda_model(start);
        ss = new_lda_suffstats(model);
    }

    sprintf(filename,"%s/000",directory);
    save_lda_model(model, filename);

    // run expectation maximization
    int i = 0;
    double likelihood, likelihood_old = 0, converged = 1, perplexity = 0, roh =0, doc_rate = 0;
    sprintf(filename, "%s/likelihood.dat", directory);
    FILE* likelihood_file = fopen(filename, "w");
    char s_perp[100];
    sprintf(s_perp, "%s/perplexity.dat", directory);
    FILE* perplexity_file = fopen(s_perp, "w");
	document** tuple = malloc(sizeof(document*) * NLANGUAGES);
    num_run = NDOCUMENTS /MINIBATCH;
    int nr =0, begin = 0, end = 0, tokens = 0;

    for (nr =0; nr <= num_run; nr++)
    {
        sub_begin =clock();
        begin = nr * MINIBATCH;
        end = (nr + 1) * MINIBATCH > NDOCUMENTS ? NDOCUMENTS : (nr + 1) * MINIBATCH;
        i++; printf("**** em iteration %d ****\n", nr);
        printf("begin:%d end:%d\n", begin, end);
        roh = pow(TAU + end - begin, -KAPPA);
        doc_rate = 1.0 * NDOCUMENTS/(end -begin);
        online_initialize_ss(ss, model, roh);
        // e-step
        tokens = 0;
        likelihood = 0;
        for (d = begin; d < end; d++)
        {
            for(l = 0; l < NLANGUAGES; l++){
                tuple[l] = &(m_corpus[l]->docs[d]);
                tokens += m_corpus[l]->docs[d].total;
            }
            likelihood += doc_e_step(tuple,var_gamma[d], phi, model, ss);
        }
        // m-step

        perplexity = exp(-likelihood/tokens);
        online_lda_mle(model, ss, roh, ESTIMATE_ALPHA, ESTIMATE_ETA, NDOCUMENTS);
        // check for convergence

        converged = fabs((likelihood_old - likelihood) / (likelihood_old));
        printf("likelihood, perplexity: %f, %f %f\n", likelihood, perplexity);
        if (converged < 0) VAR_MAX_ITER = VAR_MAX_ITER * 2;
        likelihood_old = likelihood;

        // output model and likelihood

        fprintf(likelihood_file, "%10.10f\t%5.5e\n", likelihood, converged);
        fflush(likelihood_file);
        fprintf(perplexity_file, "%10.10f\n", perplexity);
        fflush(perplexity_file);
        sprintf(filename,"%s/%03d",directory, i);
        save_lda_model(model, filename);
        sprintf(filename,"%s/%03d.gamma",directory, i);
        online_save_gamma(filename, var_gamma, begin, end, NTOPICS);
        sub_stop = clock();
        printf("the %d-th iteration takes around %d s.\n", i, (int)(sub_stop - sub_begin)/CLOCKS_PER_SEC);

    }


    // output the final model

    sprintf(filename,"%s/final",directory);
    save_lda_model(model, filename);
    sprintf(filename,"%s/final.gamma",directory);
    save_gamma(filename, var_gamma, NDOCUMENTS, model->num_topics);

    // output the word assignments (for visualization)

    //sprintf(filename, "%s/word-assignments.dat", directory);
    FILE* w_asgn_file[NLANGUAGES];
    char file_names[NLANGUAGES][100];
    for(l = 0; l < NLANGUAGES; l++){
        sprintf(file_names[l], "%s/lan_%d_word-assignments.dat", directory, l);
        w_asgn_file[l]= fopen(file_names[l], "w");
    }
    likelihood =0;
    for (d = 0; d < NDOCUMENTS; d++)
    {
        if ((d % 100) == 0) printf("final e step document %d\n",d);

        for(l = 0; l < NLANGUAGES; l++){
            tuple[l] = &(m_corpus[l]->docs[d]);
        }
        likelihood += lda_inference(tuple, model, (var_gamma[d]), phi);

        write_word_assignment(w_asgn_file, tuple, phi, model);
    }
    perplexity = exp(-likelihood/tokens);

    fprintf(likelihood_file, "%10.10f\t%5.5e\n", likelihood, converged);
    fflush(likelihood_file);
    fprintf(perplexity_file, "%10.10f\n", perplexity);
    fflush(perplexity_file);
    for(l = 0; l < NLANGUAGES; l++){

        fclose(w_asgn_file[l]);
    }
    fclose(likelihood_file);
    fclose(perplexity_file);

    //release model
    free_lda_model(model);
    //release vargamma
    for (d = 0; d < NDOCUMENTS; d++)
        free(var_gamma[d]);
    free(var_gamma);


    //release phi
    for(l = 0; l < NLANGUAGES; l++){
        int max_length = max_corpus_length(m_corpus[l]);
	    for (n = 0; n < max_length; n++)
            free(phi[l][n]);
        free(phi[l]);
    }
    free(phi);

    //release ss
    free(ss->alpha_suffstats);
    for (l =0; l < NLANGUAGES; l++)
    {
        for (k =0; k < NTOPICS; k++)
        {
            free(ss->class_word[l][k]);
        }
        free(ss->class_word[l]);
        free(ss->class_total[l]);
    }
    free(ss->class_total);
    free(ss->class_word);
    free(ss);
    clk_stop = clock();
    printf("the total estimation takes around %d s.\n", (int)(clk_stop - clk_begin)/CLOCKS_PER_SEC);
}

int main(int argc, char* argv[])
{
    // (est / inf) alpha k settings data (random / seed/ model) (directory / out)

    corpus** m_corpus;
    m_corpus = malloc(sizeof(corpus*));
    long t1;
    (void) time(&t1);
    seedMT(t1);
    int l, d;
    seedMT(4357U);

    if (argc > 1)
    {
        if (strcmp(argv[1], "batch")==0)
        {
            INITIAL_ALPHA = atof(argv[2]);
            INITIAL_ETA = atof(argv[3]);
            NTOPICS = atoi(argv[4]);
            read_settings(argv[5]);
            make_directory(argv[6]);
            NLANGUAGES = atoi(argv[8]);

            //allocate the corpus
            m_corpus = malloc(sizeof(corpus*)*NLANGUAGES);
            for(l = 0; l < NLANGUAGES; l++){
                m_corpus[l] = read_data(argv[ 9 + l]);
            }

            NDOCUMENTS = m_corpus[0]->num_docs;
            run_em(argv[7], argv[6], m_corpus);

            //release corpora
            for(l = 0; l < NLANGUAGES; l++){
                for (d =0; d < NDOCUMENTS; d++) {
                    free(m_corpus[l]->docs[d].words);
                    free(m_corpus[l]->docs[d].counts);
                }
                free(m_corpus[l]->docs);
            }
            free(m_corpus);
        }

        if (strcmp(argv[1], "online")==0)
        {
            INITIAL_ALPHA = atof(argv[2]);
            INITIAL_ETA = atof(argv[3]);
            TAU = atof(argv[4]);
            KAPPA = atof(argv[5]);
            MINIBATCH = atoi(argv[6]);
            NTOPICS = atoi(argv[7]);

            read_settings(argv[8]);
            make_directory(argv[9]);
            NLANGUAGES = atoi(argv[11]);

            //allocate the corpus
            m_corpus = malloc(sizeof(corpus*)*NLANGUAGES);
            for(l = 0; l < NLANGUAGES; l++){
                m_corpus[l] = read_data(argv[ 12 + l]);
            }

            NDOCUMENTS = m_corpus[0]->num_docs;
            online(argv[10], argv[9], m_corpus);

            //release corpora
            for(l = 0; l < NLANGUAGES; l++){
                for (d =0; d < NDOCUMENTS; d++) {
                    free(m_corpus[l]->docs[d].words);
                    free(m_corpus[l]->docs[d].counts);
                }
                free(m_corpus[l]->docs);
            }
            free(m_corpus);
        }




        if (strcmp(argv[1], "inf")==0)

        {
            read_settings(argv[2]);
            m_corpus = malloc(sizeof(corpus*)*NLANGUAGES);
            for(l = 0; l < NLANGUAGES; l++){
                m_corpus[l] = read_data(argv[ 7 + l ]);
            }
            NDOCUMENTS = m_corpus[0]->num_docs;
            infer(argv[3], argv[5], m_corpus);
        }
    }
    else
    {
        printf("usage : lda batch [initial alpha] [inital eta] [k] [settings] [directory] [random] [language] [data-1] [data...] [data-L]\n");
        printf("        lda online [initial alpha] [inital eta] [tau] [kappa] [minibatch] [k] [settings] [directory] [random] [language] [data-1] [data...] [data-L]\n");

    }
    return(0);
}



