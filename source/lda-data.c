// (C) Copyright 2004, David M. Blei (blei [at] cs [dot] cmu [dot] edu)

// This file is part of LDA-C.

// LDA-C is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your
// option) any later version.

// LDA-C is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

#include "lda-data.h"
corpus* read_data_lan(char* data_filename, int lan)
{
    FILE *fileptr;
    int length, count, word, n, nd, nw;
    corpus* c;

    printf("reading data from %s\n", data_filename);
    c = malloc(sizeof(corpus));
    c->docs = 0;
    c->num_terms = 0;
    c->num_docs = 0;
    fileptr = fopen(data_filename, "r");
    nd = 0; nw = 0;
    while ((fscanf(fileptr, "%10d", &length) != EOF))
    {
        c->docs = (document*) realloc(c->docs, sizeof(document)*(nd+1));
        c->docs[nd].length = length;
        c->docs[nd].total = 0;
        c->docs[nd].words = malloc(sizeof(int)*length);
        c->docs[nd].counts = malloc(sizeof(int)*length);
        c->docs[nd].language = lan;
        for (n = 0; n < length; n++)
        {
            fscanf(fileptr, "%10d:%10d", &word, &count);
            word = word - OFFSET;
            c->docs[nd].words[n] = word;
            c->docs[nd].counts[n] = count;
            c->docs[nd].total += count;
            if (word >= nw) { nw = word + 1; }
        }
        nd++;
    }
    fclose(fileptr);
    c->num_docs = nd;
    c->num_terms = nw;
    printf("language id: %d\n", lan);
    printf("number of docs    : %d\n", nd);
    printf("number of terms   : %d\n", nw);
    return(c);
}

corpus* read_data(char* data_filename)
{
    FILE *fileptr;
    int length, count, word, n, nd, nw;
    corpus* c;

    printf("reading data from %s\n", data_filename);
    c = malloc(sizeof(corpus));
    c->docs = 0;
    c->num_terms = 0;
    c->num_docs = 0;
    fileptr = fopen(data_filename, "r");
    nd = 0; nw = 0;
    while ((fscanf(fileptr, "%10d", &length) != EOF))
    {
        c->docs = (document*) realloc(c->docs, sizeof(document)*(nd+1));
        c->docs[nd].length = length;
        c->docs[nd].total = 0;
        c->docs[nd].words = malloc(sizeof(int)*length);
        c->docs[nd].counts = malloc(sizeof(int)*length);
        for (n = 0; n < length; n++)
        {
            fscanf(fileptr, "%10d:%10d", &word, &count);
            word = word - OFFSET;
            c->docs[nd].words[n] = word;
            c->docs[nd].counts[n] = count;
            c->docs[nd].total += count;
            if (word >= nw) { nw = word + 1; }
        }
        nd++;
    }
    fclose(fileptr);
    c->num_docs = nd;
    c->num_terms = nw;
    printf("number of docs    : %d\n", nd);
    printf("number of terms   : %d\n", nw);
    return(c);
}

int max_corpus_length(corpus* c)
{
    int n, max = 0;
    for (n = 0; n < c->num_docs; n++)
        if (c->docs[n].length > max) max = c->docs[n].length;
    return(max);
}

/*
 * writes the word assignments line for a document to a file
 *
 */

void write_word_assignment(FILE** f, document** doc, double*** phi, lda_model* model)
{
    int n, l;

    for(l = 0; l < model->num_languages; l++){
        fprintf(f[l], "%5d",doc[l]->length);
        for (n = 0; n < doc[l]->length; n++)
        {
            fprintf(f[l], " %08d:%05d",
                    doc[l]->words[n], argmax(phi[l][n], model->num_topics));
        }
        fprintf(f[l], "\n");
        fflush(f[l]);
    }

}


/*
 * saves the gamma parameters of the current dataset
 *
 */

void save_gamma(char* filename, double** gamma, int num_docs, int num_topics)
{
    FILE* fileptr;
    int d, k;
    fileptr = fopen(filename, "w");

    for (d = 0; d < num_docs; d++)
    {
        for (k = 0; k < num_topics; k++)
        {
            fprintf(fileptr, " %5.10f", gamma[d][k]);
        }
        fprintf(fileptr, "\n");
    }
    fclose(fileptr);
}

void corr_save_gamma(char* filename, double*** gamma, int num_docs, int num_topics, int num_languages)
{
    FILE* fileptr;
    int d, k, l;
    char sfile_name[100];

    for (l = 0; l < num_languages; l++)
    {
        sprintf(sfile_name, "%s_lan_%d.gamma", filename, l);

        fileptr = fopen(sfile_name, "w");
        for (d = 0; d < num_docs; d++)
        {
            for (k = 0; k < num_topics; k++)
            {
                fprintf(fileptr, " %5.10f", gamma[d][l][k]);
            }
            fprintf(fileptr, "\n");
        }
        fclose(fileptr);

    }

}

void ol_corr_save_gamma(char* filename, double*** gamma, int begin, int end, int num_topics, int num_languages)
{
    FILE* fileptr;
    int d, k, l;
    char sfile_name[100];

    for (l = 0; l < num_languages; l++)
    {
        sprintf(sfile_name, "%s_lan_%d.gamma", filename, l);

        fileptr = fopen(sfile_name, "w");
        for (d = begin; d < end; d++)
        {
            for (k = 0; k < num_topics; k++)
            {
                fprintf(fileptr, " %5.10f", gamma[d][l][k]);
            }
            fprintf(fileptr, "\n");
        }
        fclose(fileptr);

    }
}

void online_save_gamma(char* filename, double** gamma, int begin, int end, int num_topics)
{
    FILE* fileptr;
    int d, k;
    fileptr = fopen(filename, "w");

    for (d = begin ; d < end; d++)
    {
        for (k = 0; k < num_topics; k++)
        {
            fprintf(fileptr, " %5.10f", gamma[d][k]);
        }
        fprintf(fileptr, "\n");
    }
    fclose(fileptr);
}

